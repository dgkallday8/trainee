import React from 'react'

import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'

import Characters from './components/characters/Characters'

import { Header } from './components/header/Header'
import { Main } from './components/homepage/Main'

import { NotFound } from './components/notfound/NotFound'

export function App(): React.ReactElement {
  return (
    <Router>
      <Header />
      <Switch>
        <Route exact path="/">
          <Main />
        </Route>
        <Route exact path="/character/add">
          <Characters />
        </Route>
        <Route exact path="/character/:id">
          <Characters />
        </Route>
        <Route exact path="/character">
          <Characters />
        </Route>
        <Route exact path="/not_found">
          <NotFound />
        </Route>
        <Redirect to="/not_found" />
      </Switch>
    </Router>
  )
}
