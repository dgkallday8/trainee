import React from 'react'
import { Link } from 'react-router-dom'

export function Button({
  className,
  pathTo,
  textBtn,
}: {
  className: string
  pathTo: string
  textBtn: string
}): React.ReactElement {
  return (
    <button className={className}>
      <Link to={pathTo}>{textBtn}</Link>
    </button>
  )
}
