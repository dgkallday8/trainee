import { PayloadAction } from '@reduxjs/toolkit'
import React from 'react'
import { withRouter } from 'react-router'
import { RouteComponentProps } from 'react-router-dom'

import { startAppFetch } from '../../store/main/main.creator'
import {
  addToFiltersObj,
  IFilterObjSlice,
  setMainUrl,
  setSearchValue,
  togglePersonModal,
} from '../../store/main/main.slice'
import { useAppSelector, useAppDispatch } from '../../store/store.hook'
import { checkForValues, converterFilterObjectToString } from '../../utils'

import Cards from './cards/Cards'
import { FilterBar } from './navbar/FilterBar'
import { Pagination } from './pagination/Pagination'

import './characters.css'

function Characters(props: RouteComponentProps): React.ReactElement {
  const dispatch = useAppDispatch()
  const filtersObj = useAppSelector((state) => state.main.filtersObj)
  const activeIndex = useAppSelector((state) => state.main.activeIndex)
  const mainUrl = useAppSelector((state) => state.main.mainURL)

  const openPersonModal = (): any => dispatch(togglePersonModal(true))
  const addFilterParameterToObj = (parameter: string, value: string): PayloadAction<IFilterObjSlice> =>
    dispatch(addToFiltersObj({ parameter, value }))

  const currentPageToShow = useAppSelector((state) => state.main.currentPageToShow)
  const numberOfCardsOnSlide = useAppSelector((state) => state.main.numberOfCardsOnSlide)

  const url = `${mainUrl}&page=${activeIndex}&size=${numberOfCardsOnSlide}`

  React.useEffect(() => {
    if (props.match.path === '/character/:id' && !props.location.search) {
      const param = props.match.params as { id: number }
      const id = param.id
      const idFetch = async (): Promise<void> => {
        const response = await fetch(`http://localhost:5000/api/HARRY_POTTER/character/${id}`)
        if (response.ok) {
          openPersonModal()
        } else {
          location.replace('/not_found')
        }
      }
      void idFetch()
    }
  }, [])

  React.useEffect(() => {
    if (props.location.search) {
      const searchParameter = props.location.search
      const filterStartArray = searchParameter.split('&').slice(1, -2)
      filterStartArray.forEach((item) => {
        const itemArray = item.split('=')
        if (itemArray[0] === 'values') {
          dispatch(setSearchValue(itemArray[1]))
        } else {
          addFilterParameterToObj(itemArray[0], itemArray[1])
        }
      })
    }
  }, [])

  React.useEffect((): void => {
    //@ts-ignore
    dispatch(startAppFetch(url))
  }, [activeIndex, mainUrl])

  React.useEffect((): void => {
    const pathApi = 'http://localhost:5000/api/HARRY_POTTER'
    const partOfPath = '/character?sort=createdDate%2CASC'
    if (checkForValues(filtersObj) && !props.location.search) {
      dispatch(setMainUrl(pathApi + partOfPath))
    } else {
      dispatch(setMainUrl(pathApi + partOfPath + converterFilterObjectToString(filtersObj)))
      props.history.push(
        `${partOfPath}${converterFilterObjectToString(filtersObj)}&page=${activeIndex}&size=${numberOfCardsOnSlide}`
      )
    }
  }, [filtersObj.search.length, filtersObj.gender.length, filtersObj.race.length, filtersObj.side.length, activeIndex])

  React.useEffect(() => {
    if (props.location.search && checkForValues(filtersObj)) {
      props.history.push('')
    }
  }, [])
  return (
    <div className={'characters'}>
      <FilterBar />
      <Cards />
      {currentPageToShow.length && <Pagination />}
    </div>
  )
}
export default withRouter(Characters)
