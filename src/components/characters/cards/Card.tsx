import { useHistory } from 'react-router-dom'

import { togglePersonModal } from '../../../store/main/main.slice'
import { useAppDispatch } from '../../../store/store.hook'
import { ICharacterCard } from '../types'

import { Description } from './Description'

const Card = ({ person }: { person: ICharacterCard }): React.ReactElement => {
  const dispatch = useAppDispatch()
  const history = useHistory()
  return (
    <div
      className={'card'}
      onClick={(): void => {
        dispatch(togglePersonModal(true))
        history.push('/character/' + person.id)
      }}
    >
      <div className={'card_avatar'}>
        <img src={person.imageURL} alt={person.name} />
        <div className={'card_name'} style={{ color: person.nameColor }}>
          {person.name}
        </div>
      </div>
      <div className={'card_description'} style={{ background: person.backgroundColor, color: person.parametersColor }}>
        <Description value={'Пол'} valueProp={person.gender} />
        <Description value={'Раса'} valueProp={person.race} />
        <Description value={'Сторона'} valueProp={person.side} />
      </div>
    </div>
  )
}
export default Card
