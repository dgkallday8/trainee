import React from 'react'
import { withRouter } from 'react-router'

import { useAppSelector } from '../../../store/store.hook'
import ModalAdd from '../modal/ModalAdd'

import ModalPerson from '../modal/ModalPerson'
import { ICharacterCard } from '../types'

import Card from './Card'

export function Cards(): React.ReactElement {
  const isAddModalOpen = useAppSelector((state) => state.main.isOpenAddModal)
  const isOpenPersonCard = useAppSelector((state) => state.main.isOpenPersonCard)
  const currentPageToShow = useAppSelector((state) => state.main.currentPageToShow)

  return (
    <>
      <div className={'cards'}>
        <div className={'slide active'}>
          {currentPageToShow.map((itemOfPage: ICharacterCard, index: number): React.ReactElement => {
            return <Card person={itemOfPage} key={index} />
          })}
        </div>
      </div>
      {isAddModalOpen ? <ModalAdd /> : ''}
      {isOpenPersonCard ? <ModalPerson /> : ''}
    </>
  )
}
export default withRouter(Cards)
