import React from 'react'

export const Description = ({ value, valueProp }: { value: string; valueProp: string }): React.ReactElement => {
  return (
    <div className={'card_description_row'}>
      <span>{value}</span>
      <span>{valueProp}</span>
    </div>
  )
}
