import { TTagSpan } from './types'

export default function TagSpan({ value, color }: TTagSpan): React.ReactElement {
  return (
    <span className={'preview_modal_tag'} style={{ background: color }}>
      {value}
    </span>
  )
}
