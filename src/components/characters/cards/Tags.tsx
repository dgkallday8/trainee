import { TTags } from '../types'

import TagSpan from './TagSpan'

export const Tags = ({ tags }: TTags): React.ReactElement => {
  const arrayTags = tags.split(',')
  return (
    <div className={'preview_tags'}>
      {arrayTags[0] && <TagSpan value={arrayTags[0]} color={'#26514E'} />}
      {arrayTags[1] && <TagSpan value={arrayTags[1]} color={'#123856'} />}
      {arrayTags[2] && <TagSpan value={arrayTags[2]} color={'#512C38'} />}
    </div>
  )
}
