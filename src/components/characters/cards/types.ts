import { ICharacter } from '../types'

export type TSort = {
  sorted: boolean
  unsorted: boolean
  empty: boolean
}

export type TDataBackend = {
  content: ICharacter[]
  empty: boolean
  first: boolean
  last: boolean
  number: number
  numberOfElements: boolean
  pageable: {
    pageNumber: number
    pageSize: number
    offset: number
    paged: boolean
    unpaged: boolean
    sort: TSort
  }
  size: number
  sort: TSort
  totalElements: number
  totalPages: number
}
export type TTagSpan = {
  value: any
  color: string
}

export type TFormPerson = {
  id?: string
  name: string
  description?: string
  imageURL?: string
  nameColor: string
  backgroundColor: string
  parametersColor: string
  tags?: string
  gender: string
  race: string
  side: string
}
