import { Field } from 'react-final-form'

import { TColorAdd } from '../types'

export function AddColor({ text, name, defaultColorHEX, idValue }: TColorAdd): React.ReactElement {
  return (
    <div className={idValue}>
      <Field name={name} component="input" type="color" id={idValue} defaultValue={defaultColorHEX} />
      <label htmlFor={idValue}>{text}</label>
    </div>
  )
}
