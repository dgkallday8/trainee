import React, { KeyboardEvent } from 'react'
import { Field } from 'react-final-form'

export function AddDescriptionArea(): React.ReactElement {
  const maxLength = 100
  const [length, setLength] = React.useState(0)

  return (
    <div className={'add_description'}>
      <label htmlFor={'textarea_desc'}>Добавить описание</label>
      <p className={'desc_counter'} style={{ color: length === maxLength ? 'red' : '' }}>
        {length}/{maxLength}
      </p>
      <Field
        name="description"
        component="textarea"
        id="textarea_desc"
        maxLength={maxLength}
        onKeyUp={(e: KeyboardEvent): void => {
          const target = e.target as HTMLInputElement
          setLength(target.value.length)
        }}
      />
    </div>
  )
}
