import React from 'react'
import { Field } from 'react-final-form'

export function AddName(): React.ReactElement {
  const required = (value: string): string | undefined => (value ? undefined : 'Введите имя')
  return (
    <Field
      name="name"
      validate={required}
      render={({ input, meta }): React.ReactElement => (
        <div className={'add_name'} style={{ position: 'relative' }}>
          <label htmlFor="add_name">Добавить имя</label>
          <input id="add_name" type="text" {...input} placeholder="" />
          {meta.touched && meta.error && (
            <span style={{ color: 'red', position: 'absolute', right: 0, top: -5 }}>{meta.error}</span>
          )}
        </div>
      )}
    />
  )
}
