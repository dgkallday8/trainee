import React from 'react'
import { Field } from 'react-final-form'

import { TParamAdd } from '../types'

export function AddParam({ nameParam, textParam }: TParamAdd): JSX.Element {
  const required = (value: string): string | undefined => (value ? undefined : 'Заполните')
  return (
    <Field
      name={nameParam}
      validate={required}
      render={({ input, meta }): React.ReactElement => (
        <div className={`add_${nameParam} add_param`} style={{ position: 'relative' }}>
          <label htmlFor={`add_${nameParam}`}>{textParam}</label>
          <input id={`add_${nameParam}`} type="text" {...input} placeholder="" />
          {meta.touched && meta.error && (
            <span style={{ color: 'red', position: 'absolute', right: 0, fontSize: 10 }}>{meta.error}</span>
          )}
        </div>
      )}
    />
  )
}
