import React from 'react'
import { Field, useFormState } from 'react-final-form'

import addIcon from '../../../assets/img/icon_add.svg'
import { TPhotoAdd } from '../types'
export function AddPhoto({ imageLink, setIMG, setFormState }: TPhotoAdd): React.ReactElement {
  const formObject = useFormState()
  return (
    <div className="add_photo">
      <p className="add_photo_text">Добавить фото</p>
      <div
        className="add_photo_preview"
        style={{ background: imageLink ? `url(${imageLink})` : '#b09a81', backgroundSize: 'cover' }}
      >
        {!imageLink && <img src={addIcon} alt="add_icon" className={'add_icon_img'} />}
        {!imageLink && <label htmlFor="add_file">URL изображения</label>}
        <Field
          name="imgURL"
          component="input"
          type="file"
          id="add_file"
          value={imageLink}
          accept="image/*"
          onChange={(e: React.ChangeEvent): void => {
            const input = e.target as HTMLInputElement
            if (input.files?.length) {
              const file = input.files[0]
              const reader: FileReader = new FileReader()
              reader.readAsDataURL(file)
              reader.onload = (): void => {
                const img = reader.result as string
                setIMG(img)
                setFormState({ imageURL: reader.result })
              }
            }
          }}
        />
      </div>
      <div className="add_buttons">
        <button
          className="change_btn"
          onClick={(e: React.MouseEvent): void => {
            e.preventDefault()
            document.getElementById('add_file')?.click()
          }}
        >
          Изменить
        </button>
        <button
          className="save_btn"
          onClick={(e: React.MouseEvent): void => {
            e.preventDefault()
            console.log(formObject.values)
          }}
        >
          Сохранить
        </button>
      </div>
    </div>
  )
}
