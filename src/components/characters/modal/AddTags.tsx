import React from 'react'
import { Field } from 'react-final-form'

export function AddTags(): React.ReactElement {
  const [length, setLength] = React.useState(0)
  const maxCountTag = 3

  const tagsLengthHandler = (e: InputEvent): void => {
    const target = e.target as HTMLInputElement
    const tagValue = target.value.trim()
    const tagArray = tagValue.split(',')
    setLength(tagArray[0] === '' ? 0 : tagArray.length)
    if (tagArray.length > maxCountTag) {
      target.setAttribute('maxLength', 'true')
      target.style.background = 'red'
    } else {
      target.style.background = ''
    }
  }

  return (
    <div className={'add_tags'}>
      <label htmlFor="add_tags">Добавить тэги</label>
      <p className="tags_counter" style={{ color: length > 3 ? 'red' : '' }}>
        {length}/{maxCountTag}
      </p>
      <Field name="tags" component="input" type="text" id="add_tags" onKeyUp={tagsLengthHandler} />
    </div>
  )
}
