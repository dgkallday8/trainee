import { useFormState } from 'react-final-form'

import { Description } from '../cards/Description'
import './card_view.css'
import { Tags } from '../cards/Tags'
import { TFormPerson } from '../cards/types'
import { TCardView } from '../types'

export function CardViewV1({ imageLink }: TCardView): JSX.Element {
  const form = useFormState()
  const personInfo = form.values as TFormPerson
  return (
    <div className={'preview_v actve_preview_block'} id={'preview_window_1'}>
      <div className={'preview_v1_left'}>
        <h1 className={'preview_card_name'} style={{ color: personInfo.nameColor }}>
          {personInfo.name}
        </h1>
        <div
          className={'preview_card_description'}
          style={{ background: personInfo.backgroundColor, color: personInfo.parametersColor }}
        >
          <Description value={'Пол'} valueProp={personInfo.gender} />
          <Description value={'Раса'} valueProp={personInfo.race} />
          <Description value={'Сторона'} valueProp={personInfo.side} />
        </div>
        <div className={'preview_card_text'}>{personInfo.description}</div>
      </div>
      <div className={'preview_v1_right'}>
        {imageLink && <img src={imageLink} alt={personInfo.name} />}
        {personInfo.tags ? <Tags tags={personInfo.tags} /> : ''}
      </div>
    </div>
  )
}
