import { useFormState } from 'react-final-form'

import { Description } from '../cards/Description'
import { ICharacterCard, TCardView } from '../types'

import './card_view.css'

export function CardViewV2({ imageLink }: TCardView): JSX.Element {
  const form = useFormState()
  const personInfo = form.values as ICharacterCard

  return (
    <div className={'preview_v'} id={'preview_window_2'}>
      <div className={'preview_v2'}>
        <div className={'preview_v2_avatar'}>
          {imageLink ? <img src={imageLink} alt={personInfo.name} /> : ''}
          <div className={'preview_v2_card_name'} style={{ color: personInfo.nameColor }}>
            {personInfo.name}
          </div>
        </div>
        <div
          className={'preview_v2_description'}
          style={{ background: personInfo.backgroundColor, color: personInfo.parametersColor }}
        >
          <Description value={'Пол'} valueProp={personInfo.gender} />
          <Description value={'Раса'} valueProp={personInfo.race} />
          <Description value={'Сторона'} valueProp={personInfo.side} />
        </div>
      </div>
    </div>
  )
}
