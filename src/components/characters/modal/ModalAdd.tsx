/* eslint-disable react/no-multi-comp */
import { PayloadAction } from '@reduxjs/toolkit'
import React from 'react'
import { Form } from 'react-final-form'
import { withRouter } from 'react-router'
import { RouteComponentProps } from 'react-router-dom'

import closeIcon from '../../../assets/img/icon_close.svg'
import { setActiveIndex, toggleAddModal } from '../../../store/main/main.slice'
import { useAppDispatch } from '../../../store/store.hook'

import { postRequest, setActiveToAndRemoveFrom } from '../../../utils'

import { ICharacterCard } from '../types'

import { AddColor } from './AddColor'
import { AddDescriptionArea } from './AddDescriptionArea'
import { AddName } from './AddName'
import { AddParam } from './AddParam'
import { AddPhoto } from './AddPhoto'
import { AddTags } from './AddTags'
import { CardViewV1 } from './CardViewV1'
import { CardViewV2 } from './CardViewV2'

import './addModal.css'

function ModalAdd(props: RouteComponentProps): React.ReactElement {
  const dispatch = useAppDispatch()
  const closeAddModalHandler = (): PayloadAction<boolean> => dispatch(toggleAddModal(false))
  const routingProps = props
  const closeModal = (): void => {
    routingProps.history.push('/character')
    closeAddModalHandler()
  }
  const chooseViewOne = (): void => {
    setActiveToAndRemoveFrom(1, 2)
  }
  const chooseViewTwo = (): void => {
    setActiveToAndRemoveFrom(2, 1)
  }
  const [tempImgUrl, setTempImgUrl] = React.useState('')
  const [formState, setFormState] = React.useState({
    name: '',
    description: '',
    imageURL: tempImgUrl,
    nameColor: '#FFFFFF',
    backgroundColor: '#462929',
    parametersColor: '#FFFFFF',
    tags: '',
    gender: '',
    race: '',
    side: '',
  })

  const ModalAddForm = (): React.ReactElement => {
    return (
      <Form
        onSubmit={(values): void => console.log(values)}
        initialValues={formState}
        render={({ handleSubmit, values }): React.ReactElement => (
          <form onSubmit={handleSubmit} className={'modal_add_window'}>
            <div className="modal_add_left">
              <div className="inputs_block">
                <AddName />
                <div className="add_params">
                  <AddParam nameParam="gender" textParam="Пол" />
                  <AddParam nameParam="race" textParam="Раса" />
                  <AddParam nameParam="side" textParam="Сторона" />
                </div>
                <AddDescriptionArea />
                <AddTags />
                <div className="add_photo_and_color">
                  <AddPhoto imageLink={tempImgUrl} setIMG={setTempImgUrl} setFormState={setFormState} />
                  <div className="choose_color">
                    <p className="choose_color_text">Выбрать цвет</p>
                    <AddColor
                      text={'Цвет имени'}
                      name={'nameColor'}
                      defaultColorHEX={'#ffffff'}
                      idValue={'color_name'}
                    />
                    <AddColor
                      text={'Цвет фона параметров'}
                      name={'backgroundColor'}
                      defaultColorHEX={'#462929'}
                      idValue={'color_background_params'}
                    />
                    <AddColor
                      text={'Цвет параметров'}
                      name={'parametersColor'}
                      defaultColorHEX={'#ffffff'}
                      idValue={'color_params'}
                    />
                  </div>
                </div>
              </div>
            </div>

            <hr className={'delimiter'} />

            <div className="modal_add_right">
              <div className="preview">
                <p className="preview_text">Предварительный просмотр</p>
                <div className="close">
                  <img src={closeIcon} alt="close" onClick={closeModal} />
                </div>
                <div className="preview_choose">
                  <p id={'view_1'} className={'choose_view active_view'} onClick={chooseViewOne}>
                    Вид 1
                  </p>
                  <p id={'view_2'} className={'choose_view'} onClick={chooseViewTwo}>
                    Вид 2
                  </p>
                </div>

                <div className="preview_block">
                  <CardViewV1 imageLink={tempImgUrl} />
                  <CardViewV2 imageLink={tempImgUrl} />
                </div>

                <div className="preview_slider">
                  <div className="preview_arrow preview_arrow-left" onClick={chooseViewOne} />
                  <div className="preview_dots">
                    <div id={'dot_1'} className={'preview_dot active-dot'} onClick={chooseViewOne} />
                    <div id={'dot_2'} className={'preview_dot'} onClick={chooseViewTwo} />
                  </div>
                  <div className="preview_arrow preview_arrow-right" onClick={chooseViewTwo} />
                </div>

                <button
                  className="button_modal-save"
                  onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>): void => {
                    e.preventDefault()
                    const url = 'http://localhost:5000/api/HARRY_POTTER/character'
                    if (values.name && values.gender && values.race && values.side && values.imageURL) {
                      postRequest(url, values as ICharacterCard)
                      closeModal()
                      alert('Успешно отправлено!')
                    } else {
                      alert('Укажите имя, параметры и установите изображение!')
                    }
                    dispatch(setActiveIndex(0))
                  }}
                >
                  <a href="#">Сохранить</a>
                </button>
              </div>
            </div>
          </form>
        )}
      ></Form>
    )
  }

  return (
    <div
      className={'modal_add'}
      onClick={(e: React.MouseEvent): void => {
        const target = e.target as HTMLElement
        if (target.classList.contains('modal_add')) {
          closeModal()
        }
      }}
    >
      <ModalAddForm />
    </div>
  )
}
export default withRouter(ModalAdd)
