import { PayloadAction } from '@reduxjs/toolkit'
import React from 'react'
import { withRouter } from 'react-router'
import { RouteComponentProps } from 'react-router-dom'

import { toggleAddModal } from '../../../store/main/main.slice'
import { useAppDispatch } from '../../../store/store.hook'

import './modal.css'
export function ModalAddButton(props: RouteComponentProps): React.ReactElement {
  const dispatch = useAppDispatch()
  const openAddModalHandler = (): PayloadAction<boolean> => dispatch(toggleAddModal(true))
  const openModal = (): void => {
    props.history.push('/character/add')
    openAddModalHandler()
  }
  return (
    <div className={'button-add'} onClick={openModal}>
      <p>Добавить</p>
      <p>+</p>
    </div>
  )
}
export default withRouter(ModalAddButton)
