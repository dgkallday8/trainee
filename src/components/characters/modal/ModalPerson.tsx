import React from 'react'
import { RouteComponentProps, withRouter } from 'react-router-dom'

import { togglePersonModal } from '../../../store/main/main.slice'
import { useAppDispatch } from '../../../store/store.hook'
import { ICharacter } from '../types'

function ModalPerson(props: RouteComponentProps): React.ReactElement {
  const [person, setPerson] = React.useState<ICharacter>()
  const dispatch = useAppDispatch()

  const closeModal = (): void => {
    dispatch(togglePersonModal(false))
    props.history.push('/character')
  }
  type TParams = {
    id: string
  }
  React.useEffect(() => {
    const params = props.match.params as TParams
    if (params.id) {
      const location = props.location
      const personId = location.pathname.split('character/')[1]
      const url = `http://localhost:5000/api/HARRY_POTTER/character/${personId}`
      const getPerson = async (): Promise<void> => {
        const response = await fetch(url)
        const data = (await response.json()) as ICharacter
        setPerson(data)
        props.history.push(`/character/${personId}`)
      }
      void getPerson()
    }
  }, [location.pathname])

  return (
    <div
      className={'modal_person'}
      onClick={(e: React.MouseEvent): void => {
        const target = e.target as HTMLElement
        if (target.classList.contains('modal_person')) {
          closeModal()
        }
      }}
    >
      <div className={'modal_window'}>
        <div className={'modal_description'}>
          <h1 className={'modal_title'} style={{ color: person?.nameColor }}>
            {person?.name}
          </h1>
          <div
            className={'modal_info card_description'}
            style={{ background: person?.backgroundColor, color: person?.parametersColor }}
          >
            <p className={'desc-row card_description_row'}>
              <span>Пол</span>
              <span>{person?.gender.value}</span>
            </p>
            <p className={'desc-row card_description_row'}>
              <span>Раса</span>
              <span>{person?.race.value}</span>
            </p>
            <p className={'desc-row card_description_row'}>
              <span>Сторона</span>
              <span>{person?.side.value}</span>
            </p>
          </div>
          <div className={'modal_text'}>{person?.description}</div>
        </div>

        <div className={'modal_image_block'}>
          <div className={'modal_image'}>
            <img src={person?.imageURL} alt={person?.name} />
          </div>
          <div className={'modal_close'} onClick={closeModal} />
          <div className={'modal_tags'}>
            {person?.tag1 ? (
              <span className={'modal_tag'} style={{ background: '#26514E' }}>
                {person?.tag1}
              </span>
            ) : (
              ''
            )}
            {person?.tag2 ? (
              <span className={'modal_tag'} style={{ background: '#123856' }}>
                {person?.tag2}
              </span>
            ) : (
              ''
            )}
            {person?.tag3 && (
              <span className={'modal_tag'} style={{ background: '#512C38' }}>
                {person?.tag3}
              </span>
            )}
          </div>
        </div>
      </div>
    </div>
  )
}

export default withRouter(ModalPerson)
