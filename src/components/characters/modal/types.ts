import { TParameter } from '../types'

export type TDataContentValue = {
  backgroundColor: string
  description: string | null
  id: string
  imageURL: string
  name: string
  nameColor: string
  parametersColor: string
  gender: TParameter
  race: TParameter
  side: TParameter
  tag1: string | null
  tag2: string | null
  tag3: string | null

}

