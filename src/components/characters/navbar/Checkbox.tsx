import { PayloadAction } from '@reduxjs/toolkit'
import React, { ChangeEvent } from 'react'

import {
  addToFiltersObj,
  removeFromFiltersObj,
  setActiveIndex,
  setTranslateXValue,
  IFilterObjSlice,
} from '../../../store/main/main.slice'
import { useAppDispatch, useAppSelector } from '../../../store/store.hook'

export type TCheckboxProps = {
  title: string
  checkboxId: string
  value: string
}
function Checkbox({ title, checkboxId, value }: TCheckboxProps): React.ReactElement {
  const dispatch = useAppDispatch()
  const filtersObj = useAppSelector((state) => state.main.filtersObj)
  const addFilterParameterToObj = (parameter: string, value: string): PayloadAction<IFilterObjSlice> =>
    dispatch(addToFiltersObj({ parameter, value }))
  const removeFilterParameterFromObj = (parameter: string, value: string): PayloadAction<IFilterObjSlice> =>
    dispatch(removeFromFiltersObj({ parameter, value }))

  const nullify = (): void => {
    dispatch(setActiveIndex(0))
    dispatch(setTranslateXValue(0))
  }
  const addOrRemove = (checked: boolean, paramName: string, id: string): void => {
    checked ? addFilterParameterToObj(paramName, id) : removeFilterParameterFromObj(paramName, id)
    nullify()
  }

  const checkedHandler = (e: React.ChangeEvent): void => {
    const target = e.target as HTMLInputElement
    switch (title) {
      case 'Пол':
        addOrRemove(target.checked, 'gender', target.id)
        break
      case 'Раса':
        addOrRemove(target.checked, 'race', target.id)
        break
      case 'Сторона':
        addOrRemove(target.checked, 'side', target.id)
        break
    }
  }

  return (
    <div className={'checkbox'}>
      <input
        type={'checkbox'}
        className={'checkbox_input'}
        id={`${checkboxId}`}
        value={value}
        checked={Object.values(filtersObj).some((v: string[]): boolean => v.includes(checkboxId))}
        onChange={(e: ChangeEvent): void => checkedHandler(e)}
      />
      <label htmlFor={`${checkboxId}`} className={'checkbox_label'}>
        {value}
      </label>
    </div>
  )
}
export default Checkbox
