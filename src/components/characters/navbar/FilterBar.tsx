import './filterbar.css'
import { FilterPanel } from './FilterPanel'
import { Search } from './Search'

export function FilterBar(): React.ReactElement {
  return (
    <div className={'navbar'}>
      <Search />
      <FilterPanel />
    </div>
  )
}
