import { PayloadAction } from '@reduxjs/toolkit'
import React from 'react'

import { IFiltersObj, setGenderValues, setRaceValues, setSideValues } from '../../../store/main/main.slice'

import { useAppDispatch, useAppSelector } from '../../../store/store.hook'
import ModalAddButton from '../modal/ModalAddButton'

import { Select } from './Select'

import { TFilterBar, TParametersArray } from './types'

export interface ISelect {
  nameRus: string
  nameEng: string
  arrayOfValues: TParametersArray
}
type TSelects = ISelect[]

export function FilterPanel(): React.ReactElement {
  const dispatch = useAppDispatch()
  const filtersObject: IFiltersObj = useAppSelector((state) => state.main.filtersObj)
  const genderParameter = useAppSelector((state) => state.main.genderValues)
  const raceParameter = useAppSelector((state) => state.main.raceValues)
  const sideParameter = useAppSelector((state) => state.main.sideValues)

  const getDataParameter = async ({
    parameterName,
    dispatchFunc,
  }: {
    parameterName: string
    dispatchFunc: (data: TFilterBar[]) => PayloadAction<TFilterBar[]>
  }): Promise<void> => {
    const url = `http://localhost:5000/api/HARRY_POTTER/${parameterName}`
    try {
      const response: Response = await fetch(url)
      const data = (await response.json()) as TFilterBar[]
      dispatch(dispatchFunc(data))
    } catch (err) {
      throw new Error(`ERROR: FilterPanel.tsx ${err}`)
    }
  }

  React.useEffect((): void => {
    if (Object.values(filtersObject).every((v: string[]) => !v.length)) {
      void getDataParameter({ parameterName: 'gender', dispatchFunc: setGenderValues })
      void getDataParameter({ parameterName: 'race', dispatchFunc: setRaceValues })
      void getDataParameter({ parameterName: 'side', dispatchFunc: setSideValues })
    }
  }, [])

  const selects: TSelects = [
    {
      nameRus: 'Пол',
      nameEng: 'gender',
      arrayOfValues: genderParameter,
    },
    {
      nameRus: 'Раса',
      nameEng: 'race',
      arrayOfValues: raceParameter,
    },
    {
      nameRus: 'Сторона',
      nameEng: 'side',
      arrayOfValues: sideParameter,
    },
  ]

  return (
    <div className={'buttons-panel'}>
      <div className={'filters'}>
        {selects.map((select, index) => {
          const lengthArr = filtersObject[select.nameEng] as TFilterBar[]
          return (
            <Select
              title={select.nameRus}
              selectId={`${select.nameEng}:${index}`}
              arrayOfValues={select.arrayOfValues}
              iconId={`icon${index}`}
              length={lengthArr.length}
              key={index}
            />
          )
        })}
        <ModalAddButton />
      </div>
    </div>
  )
}
