import qs from 'qs'

import searchImg from '../../../assets/img/search.svg'
import { setActiveIndex, setSearchValue, setTranslateXValue } from '../../../store/main/main.slice'
import { useAppDispatch, useAppSelector } from '../../../store/store.hook'

export function Search(): React.ReactElement {
  const dispatch = useAppDispatch()
  const filtersObj = useAppSelector((state) => state.main.filtersObj)

  const inputValueHandler = (e: React.ChangeEvent): void => {
    const target = e.target as HTMLInputElement
    const searchValue = target.value.toLowerCase().trim()

    dispatch(setSearchValue(searchValue))
    dispatch(setActiveIndex(0))
    dispatch(setTranslateXValue(0))
  }

  const searchString = filtersObj.search
  const qsObjSearch = qs.parse(searchString, { charset: 'utf-8' }) as { search: string }
  const parsedValue = Object.keys(qsObjSearch)[0]
  const idSearch = 'search'

  return (
    <div className={'search-panel'}>
      <input
        type={'search'}
        id={idSearch}
        className={'search'}
        placeholder={'Поиск'}
        value={filtersObj.search.length ? parsedValue : ''}
        onChange={(e): void => inputValueHandler(e)}
      />
      <label htmlFor={idSearch} className={'search-label'}>
        <img src={searchImg} alt={idSearch} />
      </label>
    </div>
  )
}
