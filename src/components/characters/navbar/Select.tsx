import dropIcon from '../../../assets/img/dropdown.svg'
import { selectItemClickHandler } from '../../../utils'

import Checkbox from './Checkbox'
import { TFilterBar, TParametersArray } from './types'

interface ISelect {
  title: string
  arrayOfValues: TParametersArray
  length: number
  selectId: string
  iconId: string
}

export function Select({ title, arrayOfValues, length, selectId, iconId }: ISelect): React.ReactElement {
  return (
    <div className={'select'}>
      <div className={'select_item'} onClick={(): void => selectItemClickHandler(iconId, selectId)}>
        <p>{length > 0 ? `Выбрано: ${length}` : `${title}`}</p>
        <img src={dropIcon} id={`${iconId}`} alt={'drop_icon'} />
      </div>
      <div className={'select_list hide'} id={`${selectId}`}>
        {arrayOfValues.map((value: TFilterBar, index: number) => {
          return <Checkbox title={title} checkboxId={`${value.id}`} value={value.value} key={index} />
        })}
      </div>
    </div>
  )
}
