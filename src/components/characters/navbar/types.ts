export type TFilterBar = {
  id: string
  value: string
}

export type TParametersArray = TFilterBar[]
