import { PayloadAction } from '@reduxjs/toolkit'
import React from 'react'

// import { startAppFetch } from '../../../store/main/main.creator'

// import { setActiveIndex, setTotalElements, setTranslateXValue } from '../../../store/main/main.slice'
import { setActiveIndex, setTranslateXValue } from '../../../store/main/main.slice'
import { useAppDispatch, useAppSelector } from '../../../store/store.hook'
// import { TDataBackend } from '../cards/types'
import './pagination.css'

export function Pagination(): JSX.Element {
  const dispatch = useAppDispatch()
  const activeIndex = useAppSelector((state) => state.main.activeIndex)
  const translateXValue = useAppSelector((state) => state.main.translateXValue)
  const dotsCount = useAppSelector((state) => state.main.dotsCount)
  const changeActiveIndex = (num: number): PayloadAction<number, string> => dispatch(setActiveIndex(num))
  const setTranslateValue = (num: number): PayloadAction<number, string> => dispatch(setTranslateXValue(num))
  // const setTotalElementsForShow = (num: number): PayloadAction<number, string> => dispatch(setTotalElements(num))

  // const fetchData = async (): Promise<void> => {
  //   const url = 'http://localhost:5000/api/HARRY_POTTER/character'
  //   const response: Response = await fetch(url)
  //   const data = (await response.json()) as TDataBackend
  //   setTotalElementsForShow(data.totalElements)
  // }
  // React.useEffect(() => {
  //   try {
  //     void fetchData()
  //   } catch (e) {
  //     throw new Error(`Error: ${e}`)
  //   }
  // }, [])

  // React.useEffect(() => {
  //   //@ts-ignore
  //   dispatch(startAppFetch())
  // }, [])

  const nextSlide = (activeIndex: number): void => {
    if (activeIndex === dotsCount - 1) {
      return
    }
    changeActiveIndex((activeIndex += 1))
    if (dotsCount <= 3) {
      setTranslateValue(0)
    }
    if (dotsCount === 4) {
      activeIndex > 1 && setTranslateValue(40)
      activeIndex === 1 && setTranslateValue(0)
    }
    if (dotsCount === 5) {
      activeIndex === 1 && setTranslateValue(0)
      activeIndex > 1 && setTranslateValue(40)
      activeIndex > 2 && setTranslateValue(80)
    }
  }
  const prevSlide = (activeIndex: number): void => {
    if (activeIndex <= 0) {
      return
    }
    changeActiveIndex((activeIndex -= 1))
    if (dotsCount === 4) {
      activeIndex <= 1 && setTranslateValue(0)
      activeIndex === 2 && setTranslateValue(40)
    }
    if (dotsCount === 5) {
      activeIndex === 1 && setTranslateValue(0)
      activeIndex === 2 && setTranslateValue(40)
      activeIndex === 3 && setTranslateValue(80)
    }
  }
  const setSlide = (index: number): void => {
    changeActiveIndex(index)
  }
  const dotsArray: string[] = new Array(dotsCount).fill('') as string[]

  return (
    <div className={'pagination'}>
      <div className={'arrow arrow-left'} onClick={(): void => prevSlide(activeIndex)} />
      <div className={'dots'} id={'dots'}>
        {dotsArray.map((_, index) => {
          return (
            <div className={'dot_block'} key={index} style={{ transform: `translateX(-${translateXValue}px)` }}>
              <div className={index === activeIndex ? 'dot active_dot' : 'dot'} onClick={(): void => setSlide(index)} />
            </div>
          )
        })}
      </div>
      <div className={'arrow arrow-right'} onClick={(): void => nextSlide(activeIndex)} />
    </div>
  )
}
