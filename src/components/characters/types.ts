export type TParameter = {
  id: string
  value: string
}

export interface ICharacter {
  id: string
  name: string
  description: string
  imageURL: string
  nameColor: string
  backgroundColor: string
  parametersColor: string
  tag1: string
  tag2: string
  tag3: string
  gender: TParameter
  race: TParameter
  side: TParameter
}
export interface ICharacterCard {
  id: string
  name: string
  description: string
  imageURL: string
  nameColor: string
  backgroundColor: string
  parametersColor: string
  tags?: string | null
  tag1?: string
  tag2?: string
  tag3?: string
  gender: string
  race: string
  side: string
}

export type TFilterObjectParam = [key: string, value: string | string[]]

export type TColorAdd = {
  text: string
  name: string
  defaultColorHEX: string
  idValue: string
}

export type TParamAdd = {
  nameParam: string
  textParam: string
}

export type TPhotoAdd = {
  imageLink: string
  setIMG: (value: string) => void
  setFormState: (obj: any) => void
}

export type TTags = {
  tags: string
}
export type TCardView = {
  imageLink: string
}
