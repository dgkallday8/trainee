import logo from '../../assets/img/logo.png'

import { LinkEl } from './LinkEl'
import './header.css'

export const Header = (): React.ReactElement => {
  return (
    <header className={'header'}>
      <div className={'logo'}>
        <img src={logo} alt={'logo'} />
      </div>
      <nav className={'menu'}>
        <ul className={'menu_list'}>
          <LinkEl linkPath={'/'}>Главная</LinkEl>
          <LinkEl linkPath={'/character'}>Персонажи</LinkEl>
        </ul>
      </nav>
    </header>
  )
}
