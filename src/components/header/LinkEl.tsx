import { Link } from 'react-router-dom'

export function LinkEl({ linkPath, children }: { linkPath: string; children: string }): JSX.Element {
  return (
    <li className={'menu_item'}>
      <Link to={linkPath} className={'menu_link'}>
        {children}
      </Link>
    </li>
  )
}
