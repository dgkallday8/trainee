import { Button } from '../Button'
import './main.css'

export function Main(): JSX.Element {
  return (
    <div className={'main'}>
      <div className={'content'}>
        <h1 className={'title'}>
          Найди любимого персонажа <br />
          “Гарри Поттера”
        </h1>
        <p className={'info'}>Вы сможете узнать тип героев, их способности, сильные стороны и недостатки.</p>
        <Button className={'button'} pathTo={'/character'} textBtn={'Начать'} />
      </div>
    </div>
  )
}
