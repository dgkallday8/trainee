import { Button } from '../Button'
import './notfound.css'

export function NotFound(): JSX.Element {
  return (
    <div className={'not_found'}>
      <h1 className={'title_404'}>404</h1>
      <p className={'p_404'}>Ошибка 404. Такая страница не существует либо она была удалена</p>
      <Button className={'error_button'} pathTo={'/'} textBtn={'На главную'} />
    </div>
  )
}
