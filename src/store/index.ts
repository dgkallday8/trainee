import { configureStore } from '@reduxjs/toolkit'

import mainSliceReducer from './main/main.slice'

export const store = configureStore({
  reducer: {
    main: mainSliceReducer,
  },
})
