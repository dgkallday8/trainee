import { createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios'

import { TDataBackend } from '../../components/characters/cards/types'

export const startAppFetch = createAsyncThunk('startAppFetch', async (url: string) => {
  try {
    const response = await axios.get(url)
    console.log(response)
    return response.data as TDataBackend
  } catch (e) {
    return new Error(`Какая то ошибка такая: ${e}`)
  }
})
