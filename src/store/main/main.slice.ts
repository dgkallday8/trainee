import { createSlice, PayloadAction } from '@reduxjs/toolkit'

import { TDataBackend } from '../../components/characters/cards/types'
import { TFilterBar } from '../../components/characters/navbar/types'

import { startAppFetch } from './main.creator'

export interface IFiltersObj {
  search: string
  gender: string[]
  race: string[]
  side: string[]
}
const initialFiltersObject: IFiltersObj = { search: '', gender: [], race: [], side: [] }

const initialState = {
  filtersObj: initialFiltersObject,
  activeIndex: 0,
  isOpenAddModal: false,
  isOpenPersonCard: false,
  numberOfCardsOnSlide: 3,
  dotsCount: 0,
  translateXValue: 0,
  currentPageToShow: [],
  genderValues: [] as TFilterBar[],
  raceValues: [] as TFilterBar[],
  sideValues: [] as TFilterBar[],
  mainURL: `http://localhost:5000/api/HARRY_POTTER/character?sort=createdDate%2CASC`,
  openedPersonId: '',
}

export interface IFilterObjSlice {
  parameter: string
  value: string
}
export const mainSlice = createSlice({
  name: 'main',
  initialState,
  reducers: {
    addToFiltersObj(state, action: PayloadAction<IFilterObjSlice>) {
      const array = state.filtersObj[action.payload.parameter] as string[]
      array.push(action.payload.value)
    },
    removeFromFiltersObj(state, action: PayloadAction<IFilterObjSlice>) {
      const array = state.filtersObj[action.payload.parameter] as string[]
      state.filtersObj[action.payload.parameter] = array.filter((el: string) => el !== action.payload.value)
    },
    setSearchValue(state, action: PayloadAction<string>) {
      state.filtersObj.search = action.payload
    },
    toggleAddModal(state, action: PayloadAction<boolean>) {
      state.isOpenAddModal = action.payload
    },
    togglePersonModal(state, action: PayloadAction<boolean>) {
      state.isOpenPersonCard = action.payload
    },
    setActiveIndex(state, action: PayloadAction<number>) {
      state.activeIndex = action.payload
    },
    setTranslateXValue(state, action: PayloadAction<number>) {
      state.translateXValue = action.payload
    },
    setGenderValues(state, action) {
      const arr = [] as TFilterBar[]
      const newArr = arr.concat(action.payload)
      state.genderValues = newArr
    },
    setRaceValues(state, action) {
      const arr = [] as TFilterBar[]
      const newArr = arr.concat(action.payload)
      state.raceValues = newArr
    },
    setSideValues(state, action: PayloadAction<TFilterBar[]>) {
      const arr = [] as TFilterBar[]
      const newArr = arr.concat(action.payload)
      state.sideValues = newArr
    },
    setMainUrl(state, action: PayloadAction<string>) {
      state.mainURL = action.payload
    },
  },
  extraReducers: {
    [startAppFetch.fulfilled.type]: (state, action: PayloadAction<any>) => {
      const data = action.payload as TDataBackend
      state.currentPageToShow = data.content as never[]
      state.dotsCount = data.totalPages > 5 ? 5 : data.totalPages
    },
  },
})
export const {
  addToFiltersObj,
  removeFromFiltersObj,
  setSearchValue,
  toggleAddModal,
  togglePersonModal,
  setActiveIndex,
  setTranslateXValue,
  setGenderValues,
  setRaceValues,
  setSideValues,
  setMainUrl,
} = mainSlice.actions

export default mainSlice.reducer
