import { ICharacterCard, TParameter } from './components/characters/types'
import { IFiltersObj } from './store/main/main.slice'

export function setActiveToAndRemoveFrom(numId1: number, numId2: number): void {
  const window1 = document.getElementById(`preview_window_${numId1}`)
  const window2 = document.getElementById(`preview_window_${numId2}`)
  const view1 = document.getElementById(`view_${numId1}`)
  const view2 = document.getElementById(`view_${numId2}`)
  const dot1 = document.getElementById(`dot_${numId1}`)
  const dot2 = document.getElementById(`dot_${numId2}`)
  window1?.classList.add('actve_preview_block')
  window2?.classList.remove('actve_preview_block')
  view1?.classList.add('active_view')
  view2?.classList.remove('active_view')
  dot1?.classList.add('active-dot')
  dot2?.classList.remove('active-dot')
}

type Tvalue = [string, string | []]
export const converterFilterObjectToString = (object: IFiltersObj): string => {
  const arrayOfFilters = Object.entries(object)

  const urlArray: string[] = []
  const newUrl = arrayOfFilters.filter((value: Tvalue) => {
    if (value[1].length) {
      return value
    }
  })
  newUrl.forEach((item) => {
    if (Array.isArray(item[1])) {
      const array = item[1].map((id) => {
        return `&${item[0]}=${id}`
      })
      const str = array.join('')
      urlArray.push(str)
    } else {
      const str = `&values=${item[1]}`
      urlArray.push(str)
    }
  })
  return urlArray.join('')
}

export const checkForValues = (obj: IFiltersObj): boolean => {
  return Object.values(obj).every((item: string | string[]) => !item.length)
}

interface IDataForm {
  name: string | null
  description: string
  imageURL: string | null
  nameColor: string | null
  backgroundColor: string | null
  parametersColor: string | null
  tag1: string | null
  tag2: string | null
  tag3: string | null
  gender: TParameter | null
  race: TParameter | null
  side: TParameter | null
}

function convertFormData(data: ICharacterCard): IDataForm {
  let tagsArray: string[] | null
  if (data.tags) {
    tagsArray = data.tags.split(',')
  } else {
    tagsArray = null
  }
  const submitData: IDataForm = {
    name: data.name,
    description: data.description,
    imageURL: data.imageURL,
    nameColor: data.nameColor,
    backgroundColor: data.backgroundColor,
    parametersColor: data.parametersColor,
    tag1: tagsArray ? tagsArray[0] : null,
    tag2: tagsArray ? tagsArray[1] : null,
    tag3: tagsArray ? tagsArray[2] : null,
    gender: convertGender(data.gender),
    race: convertRace(data.race),
    side: convertSide(data.side),
  }
  return submitData
}

function convertGender(val: string): TParameter | null {
  switch (val.toLowerCase().trim()) {
    case 'мужчина':
      return { id: 'ad363ad1-9879-45d7-9a01-c1e28955194b', value: 'Мужчина' }
    case 'женщина':
      return { id: '18014503-9aae-47eb-abc7-b2ca7c595eb0', value: 'Женщина' }
    default:
      return { id: '6de244eb-591b-4fbe-a023-3f33bd45db21', value: 'Неизвестен' }
  }
}
function convertRace(val: string): TParameter | null {
  switch (val.toLowerCase().trim()) {
    case 'человек':
      return { id: 'a9c7683f-9eca-421b-83a3-6e800c8d59bc', value: 'Человек' }
    case 'получеловек':
      return { id: 'bd715a7a-4b3a-45d5-98a4-721cd61ba2d8', value: 'Получеловек' }
    default:
      return { id: '5b2bcf2d-6560-4320-9603-70ad5785e8cf', value: 'Неизвестен' }
  }
}
function convertSide(val: string): TParameter | null {
  switch (val.toLowerCase().trim()) {
    case 'добро':
      return { id: 'f07aeb0c-ef58-44a9-b185-daddb3a3a340', value: 'Добро' }
    case 'зло':
      return { id: 'ad363ad1-9879-45d7-9a01-c1e28955194c', value: 'Зло' }
    default:
      return { id: 'aa26c7e3-02a1-4697-8b27-fb24ae1739dc', value: 'Неизвестен' }
  }
}

export function postRequest(url: string, data: ICharacterCard): void {
  try {
    void fetch(url, {
      method: 'POST',
      headers: {
        'content-Type': 'application/json;charser=utf-8',
      },
      body: JSON.stringify(convertFormData(data)),
    })
  } catch (e) {
    alert(e)
    throw new Error(`Ошибка: ${e}`)
  }
}

export function selectItemClickHandler(iconId: string, selectId: string): void {
  const icon = document.getElementById(iconId)
  const list = document.getElementById(selectId)
  icon?.classList.toggle('rotate')
  list?.classList.toggle('hide')
}
